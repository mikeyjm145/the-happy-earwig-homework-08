﻿using System;
using System.Web;
using System.Web.UI;

/// <summary>
///     This is the code-behind file for the Confirmation page
/// </summary>
/// <author>
///     Michael Morguarge (Hello Orsega!)
/// </author>
/// <version>
///     February  3, 2015
/// </version>
public partial class Confirm : Page
{
    /// <summary>
    ///     Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && Session["Reservation"] != null)
        {
            this.DisplayReservation();
        }
    }

    /// <summary>
    ///     Displays the reservation.
    /// </summary>
    private void DisplayReservation()
    {
        var sessionClientInformation = (ContactDetails) Session["Reservation"];

        this.lblFirstName.Text = sessionClientInformation.FirstName;
        this.lblLastName.Text = sessionClientInformation.LastName;
        this.lblEmail.Text = sessionClientInformation.Email;
        this.lblPhone.Text = sessionClientInformation.Phone;
        this.lblPreferredMethod.Text = sessionClientInformation.PreferedContact;
    }

    /// <summary>
    ///     Handles the Click event of the btnModify control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    protected void btnModify_Click(object sender, EventArgs e)
    {
        var contact = new ContactDetails
        {
            Email = this.lblEmail.Text,
            FirstName = this.lblFirstName.Text,
            LastName = this.lblLastName.Text,
            Phone = this.lblPhone.Text,
            PreferedContact = this.lblPreferredMethod.Text
        };

        Session["Reservation"] = contact;
        Response.Redirect("Contact.aspx");
    }

    /// <summary>
    ///     Handles the Click event of the btnConfirm control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        var firstNameCookie = new HttpCookie("FirstName")
        {
            Value = this.lblFirstName.Text,
            Expires = DateTime.Now.AddMinutes(10)
        };
        var lastNameCookie = new HttpCookie("LastName")
        {
            Value = this.lblLastName.Text,
            Expires = DateTime.Now.AddMinutes(10)
        };

        Response.Cookies.Add(firstNameCookie);
        Response.Cookies.Add(lastNameCookie);


        var sessionClickCount = (Convert.ToInt32(Session["Count"]));

        this.lblMessage.Text = "It took you " +
                               sessionClickCount +
                               " on Submit." +
                               "<br/>" +
                               "Thank you for your request." +
                               "<br/>" +
                               "We will get back to you within 24 hours.";
    }
}