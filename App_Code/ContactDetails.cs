﻿using System.Diagnostics;

/// <summary>
/// Summary description for ContactDetails
/// </summary>
public class ContactDetails
{
    private string _firstName;
    private string _lastName;
    private string _email;
    private string _phone;
    private string _preferedContact;

    /// <summary>
    /// Initializes a new instance of the <see cref="ContactDetails"/> class.
    /// </summary>
	public ContactDetails()
	{
	    this._email = "";
	    this._firstName = "";
	    this._lastName = "";
	    this._phone = "";
	    this._preferedContact = "";
	}

    /// <summary>
    /// Gets or sets the first name.
    /// </summary>
    /// <value>
    /// The first name.
    /// </value>
    public string FirstName 
    {
        get { return this._firstName; }
        set
        {
            Trace.Assert(value != null, "This is an invalid value for First Name");
            this._firstName = value;
        }
    }

    /// <summary>
    /// Gets or sets the last name.
    /// </summary>
    /// <value>
    /// The last name.
    /// </value>
    public string LastName
    {
        get { return this._lastName; }
        set
        {
            Trace.Assert(value != null, "This is an invalid value for Last Name");
            this._lastName = value;
        }
    }

    /// <summary>
    /// Gets or sets the email.
    /// </summary>
    /// <value>
    /// The email.
    /// </value>
    public string Email
    {
        get { return this._email; }
        set
        {
            Trace.Assert(value != null, "This is an invalid value for Email");
            this._email = value;
        }
    }

    /// <summary>
    /// Gets or sets the phone.
    /// </summary>
    /// <value>
    /// The phone.
    /// </value>
    public string Phone
    {
        get { return this._phone; }
        set
        {
            Trace.Assert(value != null, "This is an invalid value for Phone");
            this._phone = value;
        }
    }

    /// <summary>
    /// Gets or sets the prefered contact.
    /// </summary>
    /// <value>
    /// The prefered contact.
    /// </value>
    public string PreferedContact
    {
        get { return this._preferedContact; }
        set
        {
            Trace.Assert(value != null, "This is an invalid value for Prefered Contact");
            this._preferedContact = value;
        }
    }
}