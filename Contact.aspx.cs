﻿using System;
using System.Net.Mime;
using System.Web.UI;

/// <summary>
///     This is the code-behind file for the Contact page
/// </summary>
/// <author>
///     Michael Morguarge (Hello Orsega!)
/// </author>
/// <version>
///     February  3, 2015
/// </version>
public partial class Request : Page
{
    /// <summary>
    ///     Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.DisplayReservation();
        }
    }

    /// <summary>
    ///     Displays the reservation.
    /// </summary>
    private void DisplayReservation()
    {
        if (Request.Cookies["FirstName"] != null && Request.Cookies["LastName"] != null)
        {
            this.txtFirstName.Text = Request.Cookies["FirstName"].Value;
            this.txtLastName.Text = Request.Cookies["LastName"].Value;
        }
    }

    /// <summary>
    ///     Handles the Click event of the btnClear control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnClear_Click(object sender, EventArgs e)
    {
        this.txtFirstName.Text = "";
        this.txtLastName.Text = "";
        this.txtEmail.Text = "";
        this.txtPhone.Text = "";
        this.ddlPreferredMethod.SelectedIndex = 0;
        this.lblMessage.Text = "";
    }

    /// <summary>
    ///     Handles the Click event of the btnSubmit control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Session["Count"] == null)
        {
            Session["Count"] = 1;
        }
        else
        {
            var count = Convert.ToInt32(Session["Count"]);
            Session["Count"] = count + 1;
        }

        

        if (Page.IsValid)
        {
            var contact = new ContactDetails
            {
                Email = this.txtEmail.Text,
                FirstName = this.txtFirstName.Text,
                LastName = this.txtLastName.Text,
                Phone = this.txtPhone.Text,
                PreferedContact = this.ddlPreferredMethod.SelectedValue
            };

            Session["Reservation"] = contact;

            Response.Redirect("Confirm.aspx");
        }
    }
}